//Esta funcion se ejecuta solo una vez
//al iniciarse el Arduino. Aqui se colocan
//las instrucciones que tienen que ver con la configuracion.
void setup() {
  Serial.begin(9600); //Iniciamos el puerto serie
  //para comunicarse con la PC
  //a una velocidad de 9600 bauds.
}

void loop() {
  int sensorValue = analogRead(A0); //Lee el valor del conversor A/D
  Serial.println(sensorValue);
  delay(500); //Espera 500ms
}
